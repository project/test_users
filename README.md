Test Users
==========
This is a Drupal module implementing several Drush commands to manipulate test
users. This can be useful for automated tests, but also to quickly get some
users for your own testing.

Commands
--------
For more extensive usage documentation, please use `drush list` and `drush
help`.

### test-users:create
Creates a user for each role given. If you want users for all roles, pass `all`. Note that the values
passed are role machine names, but the usernames are the role labels.
You may pass a password with the --password option. Otherwise, `password` is used.
You may pass an email domain with the --email option. Otherwise, `example.com` is used.

### test-users:delete
Deletes the users created by the aforementioned command. Beware: this will delete users
with the same name as roles in the system, there are no checks to make sure the user
was in fact created by the other command.

### test-users:delete-with-role
Deletes users with a given role. This can be helpful when you have e.g. a test
suite add users with a particular role to clean them up in one go.
