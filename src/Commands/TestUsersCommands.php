<?php

namespace Drupal\test_users\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Drush\Commands\DrushCommands;
use \Exception;

/**
 * Create test users via Drush.
 */
class TestUsersCommands extends DrushCommands {

  /**
   * The argument option for generating test users of all roles.
   */
  const ROLE_ALL = 'all';

  /**
   * Create test users.
   *
   * Creates a user for each role in the system (or adds the role to an existing
   * user named after the role).
   *
   * @command test-users:create
   *
   * @param array $roles
   *    Comma-separated list of roles. Pass "all" to command to generate
   *    test users for all roles.
   *
   * @option string $password
   *   Password for all newly created test users.
   * @option string $email-domain
   *   The domain to use for email addresses.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @usage drush test-users:create all
   *   Create test users for all roles.
   * @usage drush test-users:create moderator,admin
   *    Create test users for moderator and admin role.
   * @usage drush test-users:create moderator,admin --password="mySecur3!Pass15"
   *     Create test users for moderator and admin role and with given password.
   *
   * @validate-module-enabled user
   *
   * @aliases ctu,createtestusers
   *
   */
  public function createTestUsers(array $roles = NULL, array $options = ['password' => 'password', 'email-domain' => 'example.com']) {
    $all_roles = $this->getRoles();

    foreach ($all_roles as $rid => $role) {
      // Check if the role is provided as argument.
      if (!in_array($rid, $roles)) {
        continue;
      }

      $name = $role->label();

      // If the role has no label, set the name to the role ID, which is
      // also somewhat descriptive.
      if (empty($name)) {
        $name = $rid;
      }

      $mail = Html::getClass($name) . '@' . $options['email-domain'];

      /** @var \Drupal\user\UserInterface $account */
      $account = user_load_by_name($name);
      if ($account) {
        if ($account->getEmail() !== $mail) {
          if ($rid !== RoleInterface::AUTHENTICATED_ID) {
            $account->addRole($rid);
          }
          $message = dt('Added role to existing user @name.', ['@name' => $name]);
        }
        else {
          $message = dt('User for role @role already exists.', ['@role' => $name]);
        }
      }
      else {
        $account = User::create([
          'name' => $name,
          'pass' => $options['password'],
          'mail' => $mail,
          'status' => 1,
        ]);
        if ($rid !== RoleInterface::AUTHENTICATED_ID) {
          $account->addRole($rid);
        }
        $account->enforceIsNew();

        $message = dt('Created new user for role @name.', ['@name' => $name]);
      }
      try {
        $account->save();
        $this->logger()->success($message);
      }
      catch (Exception $e) {
        $this->logger()->error(dt('Failed to create user for role @name.', ['@name' => $name]));
        throw $e;
      }
    }
  }

  /**
   * Delete test users.
   *
   * Delete users as created by test-users:create. Will ask for confirmation
   * after determining which users would be deleted. Take note that it finds
   * users exclusively based on their name matching the name of a role.
   *
   * @usage drush test-users:delete
   *   Delete test users.
   * @validate-module-enabled user
   *
   * @command test-users:delete
   * @aliases dtu,deletetestusers
   */
  public function deleteTestUsers() {
    $roles = $this->getRoles();

    $accounts = [];
    foreach ($roles as $role) {
      $name = $role->label();
      /** @var \Drupal\user\UserInterface $account */
      $account = user_load_by_name($name);
      if ($account && $account->id() > 1) {
        $accounts[$name] = $account;
      }
    }

    if (empty($accounts)) {
      $this->logger()->notice('No test users found.');
    }
    else {
      $userList = implode(PHP_EOL, array_keys($accounts));
      $msg = 'This will delete these users:' . PHP_EOL . $userList . PHP_EOL . 'Are you sure?';
      $sure = $this->io()->confirm($msg);
      if ($sure) {
        foreach ($accounts as $name => $account) {
          try {
            $account->delete();
            $this->logger()->notice('Deleted user for role: ' . $name);
          }
          catch (EntityStorageException $e) {
            $this->logger()->error('Failed to delete user for role ' . $name . '. ' . $e->getMessage());
          }
        }
      }
    }
  }

  /**
   * Delete users with a specific role.
   *
   * @param $role
   *   The role the users need to have to delete them.
   * @usage drush test-users:delete-with-role test_role
   *   Delete all users that have the role test_role.
   * @validate-module-enabled user
   *
   * @command test-users:delete-with-role
   * @aliases duwr,deleteuserswithrole
   */
  public function deleteUsersWithRole($role) {
    // The user module does not offer a way to get all users for a role itself,
    // so we use an entity query.
    $query = \Drupal::entityQuery('user')
      ->condition("roles.target_id", $role);

    $result = $query->execute();

    /** @var \Drupal\Core\Session\AccountInterface[] $accounts */
    $accounts = User::loadMultiple($result);

    if (empty($accounts)) {
      $this->logger()->notice('No users found.');
    }
    else {
      $userList = implode(PHP_EOL, array_map(function ($account) {
        return $account->getDisplayName();
      }, $accounts));
      $msg = 'This will delete these users:' . PHP_EOL . $userList . PHP_EOL . 'Are you sure?';
      $sure = $this->io()->confirm($msg);
      if ($sure) {
        foreach ($accounts as $name => $account) {
          try {
            $account->delete();
            $this->logger()->notice('Deleted user ' . $name);
          }
          catch (EntityStorageException $e) {
            $this->logger()->error('Failed to delete user ' . $name . '. ' . $e->getMessage());
          }
        }
      }
    }
  }

  /**
   * @hook validate
   *
   * @throws \InvalidArgumentException
   *   Thrown when one of the passed arguments is invalid
   */
  public function validateRoles(CommandData $commandData) {
    if (!array_key_exists('roles', $commandData->arguments())) {
      return;
    }

    $input = $commandData->input();

    // Convert the comma-separated list of roles to an array with no duplicates.
    $roles = explode(',', $input->getArgument('roles') ?? '');
    $roles = array_map('trim', $roles);
    sort($roles);
    $roles = array_unique($roles);

    // Set all available roles if the user chooses this option.
    if (in_array(static::ROLE_ALL, $roles)) {
      $roles = array_keys($this->getRoles());
    }

    // Check for invalid roles.
    $available_roles = $this->getRoles();
    $available_roles_keys = array_keys($available_roles);
    $unsupported_roles = array_diff($roles, $available_roles_keys);
    if (!empty($unsupported_roles)) {
      $message = dt('Invalid roles argument "@invalid_roles". Please choose from the following: @valid_roles', [
        '@invalid_roles' => '"' . implode('", "', $unsupported_roles) . '"',
        '@valid_roles' => '"' . implode('", "', [static::ROLE_ALL] + $available_roles_keys) . '"',
      ]);
      throw new \InvalidArgumentException($message);
    }

    // Pass array of roles to command, rather than the comma-separated string.
    $input->setArgument('roles', $roles);
  }

  /**
   * Returns the available roles.
   *
   * @return \Drupal\user\RoleInterface[]
   *   An indexed array of roles.
   *
   */
  public function getRoles() {
    return user_roles(TRUE);
  }

}
